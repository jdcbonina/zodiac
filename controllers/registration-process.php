<?php 
	session_start();

	$fullName = $_POST['fullName'];
	$birthMonth = $_POST['birthMonth'];
	$birthDay = $_POST['birthDay'];

	$zodiacSign = ["CAPRICORN", "AQUARIUS", "PISCES", "ARIES", "TAURUS", "GEMINI", "CANCER", "LEO", "VIRGO", "LIBRA", "SCORPIO", "SAGITTARIUS"];
	
	
	if ($birthMonth === "january" && $birthDay <= 19 || $birthMonth === "december" && $birthDay >= 20) {
		$_SESSION['zodiacSign'] = $zodiacSign[0];
	}
	else if ($birthMonth === "february" && $birthDay <= 18 || $birthMonth === "january" && $birthDay >= 20) {
		$_SESSION['zodiacSign'] = $zodiacSign[1];
	}
	else if ($birthMonth === "march" && $birthDay <= 20 || $birthMonth === "february" && $birthDay >= 19) {
		$_SESSION['zodiacSign'] = $zodiacSign[2];
	}
	else if ($birthMonth === "april" && $birthDay <= 19 || $birthMonth === "march" && $birthDay >= 21) {
		$_SESSION['zodiacSign'] = $zodiacSign[3];
	}
	else if ($birthMonth === "may" && $birthDay <= 20 || $birthMonth === "april" && $birthDay >= 20) {
		$_SESSION['zodiacSign'] = $zodiacSign[4];
	}
	else if ($birthMonth === "june" && $birthDay <= 20 || $birthMonth === "may" && $birthDay >= 21	) {
		$_SESSION['zodiacSign'] = $zodiacSign[5];
	}
	else if ($birthMonth === "july" && $birthDay <= 22 || $birthMonth === "june" && $birthDay >= 21) {
		$_SESSION['zodiacSign'] = $zodiacSign[6];
	}
	else if ($birthMonth === "august" && $birthDay <= 22 || $birthMonth === "july" && $birthDay >= 23) {
		$_SESSION['zodiacSign'] = $zodiacSign[7];
	}
	else if ($birthMonth === "september" && $birthDay <= 22 || $birthMonth === "august" && $birthDay >= 23) {
		$_SESSION['zodiacSign'] = $zodiacSign[8];
	}
	else if ($birthMonth === "october" && $birthDay <= 22 || $birthMonth === "september" && $birthDay >= 23) {
		$_SESSION['zodiacSign'] = $zodiacSign[9];
	}
	else if ($birthMonth === "november" && $birthDay <= 21 || $birthMonth === "october" && $birthDay >= 23) {
		$_SESSION['zodiacSign'] = $zodiacSign[10];
	}
	else if ($birthMonth === "december" && $birthDay <= 21 || $birthMonth === "november" && $birthDay >= 22) {
		$_SESSION['zodiacSign'] = $zodiacSign[11];
	}
	


	// else if($zodiacSign){
	// 	header("Location:". $_SERVER['HTTP_REFERER']);
	// 	$_SESSION['errorMsg'] = "Please Valid Birth Month";
	// }




	$_SESSION['fullName'] = $fullName;
	$_SESSION['birthMonth'] = $birthMonth;
	$_SESSION['birthDay'] = $birthDay;


	header("Location: ../views/landing-page.php");
?>