<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Movies</title>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/solar/bootstrap.min.css">
</head>
<body class="bg-success">
	<h1 class="text-center text-white my-5">WELCOME TO CHECHEBURECHE</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-3" action="controllers/registration-process.php"" method="POST">
			<div class="form-group">
				<label for="fullName">Fullname</label>
				<input type="text" name="fullName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month</label>
				<input type="text" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day</label>
				<input type="number" max="31" min="1" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-warning">Submit</button>
			</div>
					</form>
	</div>
</body>
</html>