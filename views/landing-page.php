<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Landing Page</title>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/solar/bootstrap.min.css">
</head>
<body class="bg-success text-white">
	<div class="d-flex justify-content-center align-items-center flex-column vh-100">
		<?php 
			session_start(); 
		?>
		<h1>Hello <?php echo $_SESSION['fullName']?></h1>
		<br>
		<h3>You are <?php echo $_SESSION['zodiacSign'] ?></h3>

	</div>
</body>
</html>